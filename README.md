# generate_template

template for generate

這個 項目 是 爲 generate 實現的 一些代碼 模板

# install

1. 安裝 [dart](https://www.dartlang.org/tutorials/server/get-started)

1. 安裝 [generate](https://pub.dartlang.org/packages/generate#-installing-tab-)

1. 下載本項目 `git clone https://gitlab.com/king011/generate_template.git`

1. 將 項目設置到 環境變量 DART_GENERATE_TEMPLATE