package main

import (
	"log"
	"{{package}}/cmd"
)

func main() {
	if e := cmd.Execute(); e != nil {
		log.Fatalln(e)
	}
}
