#!/bin/bash
#Program:
#       golang build scripts by generate tools
#
#       https://pub.dartlang.org/packages/generate
#       tag : v0.2.0
#       commit : 967d115575cced15e28b70d0886f2f266a6c7fc2
#       datetime : 2019-04-03 14:57:54.695509
#Email:
#       zuiwuchang@gmail.com
dir=`cd $(dirname $BASH_SOURCE) && pwd`

function check(){
	if [ "$1" != 0 ] ;then
		exit $1
	fi
}
function mkDir(){
	mkdir -p "$1"
	check $?
}

function newFile(){
	echo "$2" > "$1"
	check $?
}
function writeFile(){
	echo "$2" >> "$1"
	check $?
}
function createGoVersion(){
	mkDir $dir/version
	filename=$dir/version/version.go
	package=version


	tag=`git describe`
	if [ "$tag" == '' ];then
		tag="[unknown tag]"
	fi

	commit=`git rev-parse HEAD`
	if [ "$commit" == '' ];then
		commit="[unknow commit]"
	fi
	
	date=`date +'%Y-%m-%d %H:%M:%S'`

	echo $tag $commit
	echo $date

	newFile $filename	"package $package"
	writeFile $filename	''
	writeFile $filename	'// Tag git tag'
	writeFile $filename	"const Tag = \`$tag\`"
	writeFile $filename	'// Commit git commit'
	writeFile $filename	"const Commit = \`$commit\`"
	writeFile $filename	'// Date build datetime'
	writeFile $filename	"const Date = \`$date\`"
}

function goGRPC(){
	output=$dir
	pkg='{{package}}'
	strs=${pkg//\// }
	for _ in $strs 
	do
		output=$output/..
	done
	output=`cd $output && pwd`
	if [[ "$OSTYPE" == "msys" ]]; then
		output=${output:1:1}:${output:2}/
	else
		output=$output/
	fi
	$dir/grpc.sh go $dir/pb/ $output
}

function ShowHelp(){
	echo "help                 : show help"
	echo "l/linux   [t/tar]    : build for linux"
	echo "d/darwin  [t/tar]    : build for darwin"
	echo "w/windows [t/tar]    : build for windows"
	echo "g/grpc               : build proto to grpc"
}
case $1 in
	g|grpc)
		goGRPC
	;;

	l|linux)
		export GOOS=linux

		createGoVersion

		echo go build -ldflags "-s -w" -o bin/{{project}}
		go build -ldflags "-s -w" -o bin/{{project}}
		check $?

		if [[ $2 == tar || $2 == t ]]; then
			dst=linux.amd64.tar.gz
			if [[ $GOARCH == 386 ]];then
				dst=linux.386.tar.gz
			fi
			cd bin && tar -zcvf $dst {{project}}
		fi
	;;

	d|darwin)
		export GOOS=darwin

		createGoVersion

		echo go build -ldflags "-s -w" -o bin/{{project}}
		go build -ldflags "-s -w" -o bin/{{project}}
		check $?

		if [[ $2 == tar || $2 == t ]]; then
			dst=darwin.amd64.tar.gz
			if [[ $GOARCH == 386 ]];then
				dst=darwin.386.tar.gz
			fi
			cd bin && tar -zcvf $dst {{project}}
		fi
	;;

	w|windows)
		export GOOS=windows

		createGoVersion

		echo go build -ldflags "-s -w" -o bin/{{project}}.exe
		go build -ldflags "-s -w" -o bin/{{project}}.exe
		check $?

		if [[ $2 == tar || $2 == t ]]; then
			dst=windows.amd64.tar.gz
			if [[ $GOARCH == 386 ]];then
				dst=windows.386.tar.gz
			fi
			cd bin && tar -zcvf $dst {{project}}.exe
		fi
	;;

	*)
		ShowHelp
	;;
esac