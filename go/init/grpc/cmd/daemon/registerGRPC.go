package daemon

import (
	"context"
	grpc_example "{{package}}/protocol/example"

	"google.golang.org/grpc"
)

func registerGRPC(s *grpc.Server) {
	grpc_example.RegisterServiceServer(s, _Example{})
}

type _Example struct {
}

func (_Example) Ping(ctx context.Context, request *grpc_example.PingRequest) (response *grpc_example.PingResponse, e error) {
	response = &grpc_example.PingResponse{}
	return
}
