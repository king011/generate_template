package cmd

import (
	"crypto/tls"
	"log"
	"time"

	"context"
	grpc_example "{{package}}/protocol/example"

	"github.com/spf13/cobra"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
)

func init() {
	var addr, protocol string
	cmd := &cobra.Command{
		Use:   "client",
		Short: "run a client",
		Run: func(cmd *cobra.Command, args []string) {
			var opts []grpc.DialOption
			switch protocol {
			case "h2c":
				opts = append(opts, grpc.WithInsecure())
			case "h2":
				opts = append(opts, grpc.WithTransportCredentials(credentials.NewTLS(&tls.Config{
					InsecureSkipVerify: false,
				})))
			case "h2-skip":
				opts = append(opts, grpc.WithTransportCredentials(credentials.NewTLS(&tls.Config{
					InsecureSkipVerify: true,
				})))
			default:
				log.Fatalln("not support", protocol)
			}
			c, e := grpc.Dial(addr, opts...)
			if e != nil {
				log.Fatalln(e)
			}
			last := time.Now()
			client := grpc_example.NewServiceClient(c)
			_, e = client.Ping(context.Background(), &grpc_example.PingRequest{})
			c.Close()
			log.Println("ping", e, time.Now().Sub(last))
		},
	}
	flags := cmd.Flags()
	flags.StringVarP(&addr, "addr",
		"a",
		"localhost:6000",
		"server addr",
	)
	flags.StringVarP(&protocol, "protocol",
		"p",
		"h2-skip",
		"net protocol [h2c h2 h2-skip]",
	)
	rootCmd.AddCommand(cmd)
}
