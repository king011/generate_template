{
	GRPC:{
		Addr:":6000",
		// x509 if empty use h2c
		CertFile:"test.pem",
		KeyFile:"test.key",
	},
	Logger:{
		// zap http
		//HTTP:"localhost:20000",
		// log name
		//Filename:"logs/{{project}}.log",
		// MB
		MaxSize:    100, 
		// number of files
		MaxBackups: 3,
		// day
		MaxAge:     28,
		// level : debug info warn error dpanic panic fatal
		Level :"debug",
	},
}