package daemon

import (
	"{{package}}/configure"
	"{{package}}/logger"

	"go.uber.org/zap"
)

// Run run as deamon
func Run() {
	cnf := configure.Single()
	logger.Logger.Info("daemon running",
		zap.String("level", cnf.Logger.Level),
	)
}
